# Digital Lesson Planner [2021]

Started to create a small Digital Lesson Planner

## Working Features
- save data in hive
- add / edit / delete class
- add / edit / delete lesson
- add / edit user
- login / logout user
- export / import classes | lesson as json
- search for classes | lessons

## Todos
- App should not only handle classes and lessons
  - day -> classes -> lessons -> time per lesson
  - check for how long a lesson should take
- add date handling
- establish a server request/ response connection
  - user and storage data should be saved there
    - use hive only for local device storage
  - login check on the server

## Dependencies
- [provider](https://pub.dev/packages/provider)
- [hive](https://pub.dev/packages/hive)
- [hive_flutter](https://pub.dev/packages/hive_flutter)
- [uuid](https://pub.dev/packages/uuid)
- [path_provider](https://pub.dev/packages/path_provider)
- [permission_handler](https://pub.dev/packages/permission_handler)
- [file_picker](https://pub.dev/packages/file_picker)
- [image_picker](https://pub.dev/packages/image_picker)
- [json_annotation](https://pub.dev/packages/json_annotation)
- [flutter_secure_storage](https://pub.dev/packages/flutter_secure_storage)

## Dev Dependencies
- [hive_generator](https://pub.dev/packages/hive_generator)
- [build_runner](https://pub.dev/packages/build_runner)
- [json_serializable](https://pub.dev/packages/json_serializable)

## App Overview

### Login Screen
![Login Screen](/images/readme/login_screen.png "Login Screen")

### Overview Screen
![Overview Screen](/images/readme/overview_screen.png "Overview Screen")

![Edit Class Screen](/images/readme/edit_class_screen.png "Edit Class Screen")

![Delete Class Screen](/images/readme/delete_class_screen.png "Delete Class Screen")

### Add Lessons Screen
![Add Lessons Screen](/images/readme/add_lessons_screen.png "Add Lessons Screen")

![Delete Lesson Screen](/images/readme/delete_lesson_screen.png "Delete Lesson Screen")

### Drawer Screen
![Drawer Screen](/images/readme/drawer_screen.png "Drawer Screen")

### User Settings Screen
![User Settings Screen](/images/readme/user_settings_screen.png "User Settings Screen")

### User Settings Edit Screen
![User Settings Edit Screen](/images/readme/user_settings_edit_screen.png "User Settings Edit Screen")

### Search Screen
![Search Screen](/images/readme/search_screen.png "Search Screen")
