import 'package:flutter/material.dart';

class HeadLine extends StatelessWidget {
  final EdgeInsetsGeometry? margin;
  final TextStyle? textStyle;
  final String title;
  final BoxDecoration? leftBoxDecoration;
  final BoxDecoration? rightBoxDecoration;
  final int leftFlexWidth;
  final int middleFlexWidth;
  final int rightFlexWidth;

  const HeadLine({
    Key? key,
    required this.title,
    this.margin,
    this.textStyle,
    this.leftFlexWidth: 1,
    this.leftBoxDecoration,
    this.middleFlexWidth: 2,
    this.rightBoxDecoration,
    this.rightFlexWidth: 1,
  }) : super(key: key);

  HeadLine.standard({
    required this.title,
    this.margin: const EdgeInsets.only(top: 24.0, bottom: 24.0),
    this.textStyle:
        const TextStyle(fontSize: 26.0, fontWeight: FontWeight.bold),
    this.leftFlexWidth: 1,
    this.leftBoxDecoration: const BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.black38),
      ),
    ),
    this.middleFlexWidth: 1,
    this.rightBoxDecoration: const BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.black38),
      ),
    ),
    this.rightFlexWidth: 1,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Row(
        children: [
          Expanded(
            flex: leftFlexWidth,
            child: Container(
              decoration: leftBoxDecoration,
            ),
          ),
          Expanded(
            flex: middleFlexWidth,
            child: Center(
              child: Text(title, style: textStyle),
            ),
          ),
          Expanded(
            flex: rightFlexWidth,
            child: Container(
              decoration: rightBoxDecoration,
            ),
          ),
        ],
      ),
    );
  }
}
