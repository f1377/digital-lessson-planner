import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorageService {
  SecureStorageService();

  final FlutterSecureStorage _secureStorage = FlutterSecureStorage();
  final _accountNameController =
      TextEditingController(text: 'flutter_secure_storage_service');

  static const String _isLoggedInName = 'isLoggedIn';
  String? _getAccountName() =>
      _accountNameController.text.isEmpty ? null : _accountNameController.text;

  Future<void> writeKeyIntoStorage(String key, String value) async {
    await _secureStorage.write(
        key: key,
        value: value,
        iOptions: _getIOSOptions(),
        aOptions: _getAndroidOptions());
  }

  Future<String?> readKeyFromStorage(String key) async {
    return await _secureStorage.read(
        key: key, iOptions: _getIOSOptions(), aOptions: _getAndroidOptions());
  }

  Future<Map<String, String>> readAllKeysFromStorage() async {
    return await _secureStorage.readAll(
        iOptions: _getIOSOptions(), aOptions: _getAndroidOptions());
  }

  Future<void> deleteAll() async {
    await _secureStorage.deleteAll();
  }

  Future<void> setIsLoggedIn() async {
    await _secureStorage.write(
        key: _isLoggedInName,
        value: 'false',
        iOptions: _getIOSOptions(),
        aOptions: _getAndroidOptions());
  }

  Future<void> setIsNotLoggedIn() async {
    await _secureStorage.write(
        key: _isLoggedInName,
        value: 'false',
        iOptions: _getIOSOptions(),
        aOptions: _getAndroidOptions());
  }

  Future<bool> userIsLoggedIn() async {
    final String? isLoggedIn = await _secureStorage.read(
        key: _isLoggedInName,
        iOptions: _getIOSOptions(),
        aOptions: _getAndroidOptions());

    if (isLoggedIn != null && isLoggedIn.isNotEmpty && isLoggedIn == 'true') {
      return true;
    }

    return false;
  }

  IOSOptions _getIOSOptions() => IOSOptions(
        accountName: _getAccountName(),
      );

  AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );
}
