import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:file_picker/file_picker.dart';
import 'package:digital_lesson_planner/model/class.dart';

// source: http://www.refactord.com/guides/backup-restore-share-json-file-flutter
class Storage {
  final String _filename = "backup.json";
  final String _savePath = "/storage/emulated/0/Download/";

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/$_filename');
  }

  Future<File> get _localOwnPathFile async {
    final path = _savePath;
    return File('$path$_filename');
  }

  Future<File?> writeClasses(List<Class> classes) async {
    if (!await Permission.storage.request().isGranted ||
        !await Permission.accessMediaLocation.request().isGranted ||
        !await Permission.manageExternalStorage.request().isGranted) {
      print('permission not granted');
      return Future.value(null);
    }

    final file = await _localOwnPathFile;
    if (!await file.exists()) {
      await file.create(recursive: true);
    }

    final String encodedClasses = jsonEncode(classes);
    return file.writeAsString(encodedClasses);
  }

  Future<List<Class>?> readClasses(
      [bool local = true, File? selectedFile]) async {
    try {
      final File? file;
      if (local) {
        file = await _localOwnPathFile;
      } else {
        file = selectedFile;
      }

      if (file == null) {
        print("File not found");
        return Future.value(null);
      }

      final jsonContents = await file.readAsString();
      final List<dynamic> jsonResponse = json.decode(jsonContents);
      return jsonResponse.map((i) => Class.fromJson(i)).toList();
    } catch (e) {
      // If encountering an error, return 0
      print("error: $e");
      return Future.value(null);
    }
  }

  Future<List<Class>?> readClassesFromFilePicker() async {
    await clearCacheDir();

    final FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['json'],
    );

    if (result == null) {
      print("result is null, getting here");
      return null;
    }

    final String path = result.files.single.path!;
    final File file = File(path);
    final List<Class>? importedClasses = await readClasses(false, file);

    if (importedClasses == null) {
      print('Error: No Data Found');
      return Future.value(null);
    }

    writeClasses(importedClasses);
    return importedClasses;
  }

  // https://stackoverflow.com/a/56889940
  Future<void> clearCacheDir() async {
    Directory dir = await getTemporaryDirectory();
    dir.deleteSync(recursive: true);
    dir.create();
  }
}
