import 'dart:io';

import 'package:digital_lesson_planner/model/class.dart';
import 'package:digital_lesson_planner/model/lesson.dart';
import 'package:digital_lesson_planner/model/user.dart';
import 'package:digital_lesson_planner/services/write_read_storage_service.dart';
import 'package:hive/hive.dart';
import 'package:uuid/uuid.dart';

class DigitalLessonPlannerService {
  Box userBox;
  Box classBox;
  final Storage _storageService = Storage();

  DigitalLessonPlannerService._private({
    required this.classBox,
    required this.userBox,
  });

  static Future<DigitalLessonPlannerService> create() async {
    final users = await Hive.openBox<User>('users');
    final classes = await Hive.openBox<Class>('classes');
    return DigitalLessonPlannerService._private(
        classBox: classes, userBox: users);
  }

  Future<String?> getUserId(String username, String password) async {
    final bool userExists =
        await _chekIfUserAndPasswordExists(username, password);

    if (!userExists) {
      return null;
    }

    final Iterable<User> users = userBox.values.cast<User>();
    final User foundUser = users.firstWhere(
        (user) => user.username == username && user.password == password);

    return foundUser.id;
  }

  User getUser(String userId) {
    final Iterable<User> users = userBox.values.cast<User>();
    final User foundUser = users.firstWhere((user) => user.id == userId);

    return foundUser;
  }

  void addUser(String forename, String surname, String email, String username,
      String password, String? imagePath) {
    final String uuid = Uuid().v4();

    final newUser = User(
      id: uuid,
      forename: forename,
      surname: surname,
      email: email,
      password: password,
      username: username,
    );

    if (imagePath != null) {
      newUser.imagePath = imagePath;
    }

    final String userId = Uuid().v4();
    userBox.put(userId, newUser);
  }

  Future<void> updateUser(String userId, User updatedUser) async {
    final Iterable<User> users = userBox.values.cast<User>();
    User foundUser = users.firstWhere((user) => user.id == userId);

    foundUser.forename = updatedUser.forename;
    foundUser.surname = updatedUser.surname;
    foundUser.email = updatedUser.email;
    foundUser.username = updatedUser.username;
    foundUser.password = updatedUser.password;

    if (updatedUser.imagePath != null && updatedUser.imagePath!.isNotEmpty) {
      foundUser.imagePath = updatedUser.imagePath;
    }

    await foundUser.save();
  }

  Future<bool> _chekIfUserAndPasswordExists(
      String username, String password) async {
    final Iterable<User> users = userBox.values.cast<User>();

    for (User currentUser in users) {
      if (currentUser.username == username &&
          currentUser.password == password) {
        return true;
      }
    }

    return false;
  }

  Future<bool> chekIfUsernameExists(String username) async {
    final Iterable<User> users = userBox.values.cast<User>();

    for (User currentUser in users) {
      print('username: ${currentUser.username}');
      if (currentUser.username == username) {
        return true;
      }
    }

    return false;
  }

  Class getClass(String id) {
    return classBox.get(id);
  }

  List<Class> getClasses() {
    Iterable<Class> classes = classBox.values.cast<Class>();
    List<Class> classesList = classes.toList();

    return classesList;
  }

  Future<Class> addClass(String name) async {
    final String uuid = Uuid().v4();
    final Class newClass = Class(id: uuid, name: name);

    final classExists = await _classExists(newClass);
    if (classExists) {
      print('Class: $name already exists');

      return newClass;
    }

    await classBox.put(uuid, newClass);
    return newClass;
  }

  void updateClassname(String classId, String newName) {
    Class currentClass = classBox.get(classId);
    currentClass.name = newName;
    currentClass.save();
  }

  void deleteClass(String? id) {
    if (id != null) {
      classBox.delete(id);
    }
  }

  Future<bool> _classExists(Class newClass) async {
    Iterable<Class> classes = classBox.values.cast<Class>();

    for (Class currentClass in classes) {
      if (newClass.name == currentClass.name) {
        return true;
      }
    }

    return false;
  }

  bool _lessonExists(String classId, String lessonId) {
    final Class currentClass = classBox.get(classId);

    if (currentClass.lessons == null) {
      return false;
    }

    for (int i = 0; i < currentClass.lessons!.length; i++) {
      final Lesson lesson = currentClass.lessons![i];
      if (lesson.id == lessonId) {
        return true;
      }
    }

    return false;
  }

  void addLesson(String classId, Lesson newLesson) {
    Class currentClass = classBox.get(classId);

    if (currentClass.lessons == null) {
      currentClass.lessons = [newLesson];
    } else {
      currentClass.lessons!.add(newLesson);
    }
    currentClass.save();
  }

  void updateLesson(String classId, String lessonId, String text) {
    Class currentClass = classBox.get(classId);
    Lesson currentLesson =
        currentClass.lessons!.firstWhere((lesson) => lesson.id == lessonId);

    currentLesson.text = text;
    currentClass.save();
  }

  void addOrUpdateLesson(String classId, String lessonId, String text) {
    bool lessonExists = _lessonExists(classId, lessonId);

    if (lessonExists) {
      updateLesson(classId, lessonId, text);
    } else {
      final Lesson newLesson = Lesson(id: lessonId, text: text);
      addLesson(classId, newLesson);
    }
  }

  void deleteLesson(String classId, String lessonId) {
    Class currentClass = classBox.get(classId);
    currentClass.lessons!.removeWhere((lesson) => lesson.id == lessonId);
    currentClass.save();
  }

  Future<List<Class>> searchInClassAndLessons(String search) async {
    List<Class> foundClasses = [];
    Iterable<Class> classes = classBox.values.cast<Class>();

    // search in classes
    for (Class currentClass in classes) {
      if (search == currentClass.name) {
        foundClasses.add(currentClass);
      }
    }

    if (foundClasses.isNotEmpty) {
      return foundClasses;
    }

    // search in lessons
    for (Class currentClass in classes) {
      List<Lesson> foundLessons = [];

      if (currentClass.lessons != null) {
        final List<Lesson> lessons = currentClass.lessons!;

        for (Lesson currentLesson in lessons) {
          if (currentLesson.text.contains(search)) {
            foundLessons.add(currentLesson);
          }
        }

        if (foundLessons.isNotEmpty) {
          Class newClass = Class(
            id: currentClass.id,
            name: currentClass.name,
            lessons: foundLessons,
          );
          foundClasses.add(newClass);
        }
      }
    }

    return foundClasses;
  }

  Future<void> createHiveBackup() async {
    if (Hive.box<Class>("classes").isEmpty) {
      print("is empty, nothing to backup");
      return;
    }

    final String directoryPath = "/storage/emulated/0/Download/";
    final filename = "backup.hive";
    final String hivePath = Hive.box<Class>("classes").path!;
    final String savePath = directoryPath + filename;

    File(hivePath).copy(savePath);
  }

  Future<void> restoreHiveBackup() async {
    final String classBoxPath = classBox.path!;
    await classBox.close();

    final String directoryPath = "/storage/emulated/0/Download/";
    final String filename = "backup.hive";
    final String savePath = directoryPath + filename;

    File(savePath).copy(classBoxPath);
    classBox = await Hive.openBox('classes');
  }

  Future<bool> saveClassesAsJson(List<Class> classes) async {
    if (Hive.box<Class>("classes").isEmpty) {
      print("is empty, nothing to backup");
      return false;
    }

    _storageService.writeClasses(classes);
    return true;
  }

  Future<List<Class>?> loadClassesFromJson() async {
    List<Class>? classes = await _storageService.readClasses();
    return classes;
  }

  Future<List<Class>?> readClassesFromFilePicker() async {
    List<Class>? importedClasses =
        await _storageService.readClassesFromFilePicker();

    if (importedClasses == null) {
      print("No classes imported");
      return null;
    }

    await classBox.clear();
    for (int i = 0; i < importedClasses.length; i++) {
      final Class currentClass = importedClasses[i];
      final String uuid = currentClass.id;

      await classBox.put(uuid, currentClass);
    }

    return importedClasses;
  }

  void _printClasses(List<Class> classes) {
    for (Class currentClass in classes) {
      print("-------------- ${currentClass.name} --------------");
      print("currentClass.id: ${currentClass.id}");
      print("currentClass.name: ${currentClass.name}");
      print("currentClass.lessons: ${currentClass.lessons}");

      print("-------------- ${currentClass.name} --------------");
    }
  }
}
