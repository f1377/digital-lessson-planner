import 'package:digital_lesson_planner/screens/digital_lesson_planner_screen.dart';
import 'package:digital_lesson_planner/services/digital_lesson_planner_service.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

import 'model/user.dart';
import 'model/class.dart';
import 'model/lesson.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(UserAdapter());
  Hive.registerAdapter(ClassAdapter());
  Hive.registerAdapter(LessonAdapter());

  final digitalLessonPlannerService =
      await DigitalLessonPlannerService.create();

  runApp(
    MultiProvider(
      providers: [
        Provider(create: (BuildContext context) => digitalLessonPlannerService),
      ],
      child: DigitalLessonPlanner(),
    ),
  );
}
