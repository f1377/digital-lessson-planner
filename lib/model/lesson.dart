import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'lesson.g.dart';

@JsonSerializable()
@HiveType(typeId: 2)
class Lesson extends HiveObject {
  @HiveField(0)
  final String id;

  @HiveField(1)
  String text;

  Lesson({
    required this.id,
    required this.text,
  });

  factory Lesson.fromJson(Map<String, dynamic> json) => _$LessonFromJson(json);

  Map<String, dynamic> toJson() => _$LessonToJson(this);
}
