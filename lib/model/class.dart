import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

import 'lesson.dart';

part 'class.g.dart';

@JsonSerializable()
@HiveType(typeId: 1)
class Class extends HiveObject {
  @HiveField(0)
  final String id;

  @HiveField(1)
  String? userId;

  @HiveField(2)
  String name;

  @HiveField(3)
  List<Lesson>? lessons;

  Class({
    required this.id,
    this.userId,
    required this.name,
    this.lessons,
  });

  factory Class.fromJson(Map<String, dynamic> json) => _$ClassFromJson(json);

  Map<String, dynamic> toJson() => _$ClassToJson(this);
}
