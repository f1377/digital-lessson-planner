import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'class.dart';

part 'user.g.dart';

@JsonSerializable()
@HiveType(typeId: 0)
class User extends HiveObject {
  @HiveField(0)
  final String id;

  @HiveField(1)
  String forename;

  @HiveField(2)
  String surname;

  @HiveField(3)
  String email;

  @HiveField(4)
  String username;

  @HiveField(5)
  String password;

  @HiveField(6)
  List<Class>? classes;

  @HiveField(7)
  String? imagePath;

  User({
    required this.id,
    required this.forename,
    required this.surname,
    required this.email,
    required this.username,
    required this.password,
    this.classes,
    this.imagePath,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
