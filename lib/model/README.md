# How to generate Hive Adapter
- follow this steps to generate the needed hive adapter
- after this steps the file `class.g.dart` and `lesson.g.dart` will be generated

## Sources
- [Flutter save data to local storage with Hive NoSQL database package](https://itnext.io/flutter-save-data-to-local-storage-with-hive-nosql-database-package-8a0de834f313)
- [hive_example](https://itnext.io/flutter-save-data-to-local-storage-with-hive-nosql-database-package-8a0de834f313)

## Helping Steps

### Step 1 - Add dependencies
- add `hive_generator` and `build_runner` to the `pubspec.yam`
- check versions:
[hive_generator](https://pub.dev/packages/hive_generator/install), 
[build_runner](https://pub.dev/packages/build_runner/install)

```dart
dev_dependencies:
  hive_generator: ^1.1.1
  build_runner: ^2.1.7
```

### Step 2 - Create the Models
- create a Class model

```dart
import 'package:hive/hive.dart';

import 'lesson.dart';

part 'class.g.dart';

@HiveType(typeId: 1)
class Class extends HiveObject {
  @HiveField(0)
  final String id;

  @HiveField(1)
  final String name;

  @HiveField(2)
  List<Lesson>? lessons;

  Class({
    required this.id,
    required this.name,
    this.lessons,
  });
}
```
- create a Lesson model

```dart
import 'package:hive/hive.dart';

part 'lesson.g.dart';

@HiveType(typeId: 2)
class Lesson extends HiveObject {
  @HiveField(0)
  final String id;

  @HiveField(1)
  final String name;

  Lesson({
    required this.id,
    required this.name,
  });
}
```

### Step 3 - Generate Adapter
- go to project root folder and let flutter build the adapter
- NOTE: dont forget the `part-lines` in the models

```shell
user@user:~$ cd /path/to/project/folder
user@user:~/path/to/project/folder$ flutter packages pub run build_runner build
```

### Step 4 - Registrate Adapter
- registrate Adapter in `main.dart` file

```dart
import 'package:digital_lesson_planner/screens/digital_lesson_planner_screen.dart';
import 'package:digital_lesson_planner/services/digital_lesson_planner_service.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

import 'model/class.dart';
import 'model/lesson.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(ClassAdapter());
  Hive.registerAdapter(LessonAdapter());

  final digitalLessonPlannerService =
      await DigitalLessonPlannerService.create();

  runApp(
    MultiProvider(
      providers: [
        Provider(create: (BuildContext context) => digitalLessonPlannerService),
      ],
      child: DigitalLessonPlanner(),
    ),
  );
}
```
