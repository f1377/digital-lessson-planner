import 'package:digital_lesson_planner/screens/overview_screen.dart';
import 'package:digital_lesson_planner/screens/register_screen.dart';
import 'package:digital_lesson_planner/services/digital_lesson_planner_service.dart';
import 'package:digital_lesson_planner/services/secure_storage_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final SecureStorageService _secureStorageService = SecureStorageService();
  late final DigitalLessonPlannerService _service =
      Provider.of<DigitalLessonPlannerService>(context, listen: false);

  void _onRegister(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RegistrationScreen(),
      ),
    );
  }

  Future<void> _onLogin(BuildContext context) async {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    final String? userId = await _service.getUserId(
        _usernameController.text, _passwordController.text);

    if (userId == null) {
      showSnackBarMessage("Wrong Login!");
      return;
    }

    await _secureStorageService.writeKeyIntoStorage('userId', userId);
    await _secureStorageService.setIsLoggedIn();

    _navigateToOverviewScreen(userId);
  }

  void _navigateToOverviewScreen(String userId) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => OverviewScreen(userId: userId),
      ),
    );
  }

  void showSnackBarMessage(String message) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16.0),
                  padding: const EdgeInsets.only(
                    top: 24.0,
                    right: 24.0,
                    left: 24.0,
                  ),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.orangeAccent),
                    borderRadius: BorderRadius.circular(25.0),
                  ),
                  child: Column(
                    children: [
                      TextFormField(
                        style: TextStyle(fontWeight: FontWeight.bold),
                        controller: _usernameController,
                        decoration: InputDecoration(
                          icon: const Icon(Icons.person),
                          hintText: 'Enter a Username',
                          labelText: 'Username',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Username!';
                          }

                          return null;
                        },
                      ),
                      TextFormField(
                        style: TextStyle(fontWeight: FontWeight.bold),
                        controller: _passwordController,
                        decoration: InputDecoration(
                          icon: const Icon(Icons.password),
                          hintText: 'Enter a Password',
                          labelText: 'Password',
                        ),
                        obscureText: true,
                        enableSuggestions: false,
                        autocorrect: false,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Password!';
                          }

                          return null;
                        },
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 20.0, bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: ElevatedButton(
                                onPressed: () => _onLogin(context),
                                child: Text("Login"),
                              ),
                            ),
                            ElevatedButton(
                              onPressed: () => _onRegister(context),
                              child: Text("Register"),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
