import 'package:digital_lesson_planner/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:digital_lesson_planner/screens/overview_screen.dart';

class DigitalLessonPlanner extends StatelessWidget {
  final bool showLogin = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: showLogin ? LoginScreen() : OverviewScreen(userId: ""),
    );
  }
}
