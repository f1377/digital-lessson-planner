import 'dart:io';

import 'package:digital_lesson_planner/services/digital_lesson_planner_service.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import 'login_screen.dart';

class RegistrationScreen extends StatefulWidget {
  RegistrationScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<RegistrationScreen> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final ImagePicker _imagePicker = ImagePicker();

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _forenameController = TextEditingController();
  final TextEditingController _surnameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  File? _image = File("");

  late final DigitalLessonPlannerService _service =
      Provider.of<DigitalLessonPlannerService>(context, listen: false);

  Future<void> _loadUserImage() async {
    final XFile? pickedImage =
        await _imagePicker.pickImage(source: ImageSource.gallery);

    if (pickedImage == null) {
      print('no file selected');
      return;
    }

    setState(() {
      _image = File(pickedImage.path);
    });
  }

  void _onRegisterUser() {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    String? imagePath;

    print("username: ${_usernameController.text}");
    print("forename: ${_forenameController.text}");
    print("surname: ${_surnameController.text}");
    print("password: ${_passwordController.text}");
    print("email: ${_emailController.text}");

    if (_image!.existsSync()) {
      print("imagePath: ${_image!.path}");
      imagePath = _image!.path;
    }

    _service.addUser(
        _forenameController.text,
        _surnameController.text,
        _emailController.text,
        _usernameController.text,
        _passwordController.text,
        imagePath);

    _navigateToLoginScreen();
  }

  void _navigateToLoginScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
        title: Text("User Settings"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                GestureDetector(
                  onTap: _loadUserImage,
                  child: Container(
                    margin: const EdgeInsets.only(top: 20.0, bottom: 30.0),
                    height: 200.0,
                    width: 200.0,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38),
                      shape: BoxShape.circle,
                    ),
                    child: _image!.existsSync()
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(200),
                            child: Image.file(
                              _image!,
                              fit: BoxFit.cover,
                            ),
                          )
                        : Container(),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                      right: 8.0, bottom: 30.0, left: 8.0),
                  decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: Colors.black38)),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 40.0),
                  child: Column(
                    children: [
                      TextFormField(
                        controller: _forenameController,
                        decoration: InputDecoration(
                          icon: const Icon(Icons.perm_identity),
                          hintText: 'Enter a Forename',
                          labelText: 'Forename',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Forename!';
                          }

                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _surnameController,
                        decoration: InputDecoration(
                          icon: const Icon(Icons.person_outlined),
                          hintText: 'Enter a Surname',
                          labelText: 'Surname',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Surname!';
                          }

                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _emailController,
                        decoration: InputDecoration(
                          icon: const Icon(Icons.email),
                          hintText: 'Enter a Email',
                          labelText: 'Email',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Email!';
                          }

                          if (!RegExp(r'\S+@\S+\.\S+').hasMatch(value)) {
                            return 'Please enter a valid Email!';
                          }

                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _usernameController,
                        decoration: InputDecoration(
                          icon: const Icon(Icons.person),
                          hintText: 'Enter a Username',
                          labelText: 'Username',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Username!';
                          }

                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _passwordController,
                        decoration: InputDecoration(
                          icon: const Icon(Icons.password),
                          hintText: 'Enter a Password',
                          labelText: 'Password',
                        ),
                        obscureText: true,
                        enableSuggestions: false,
                        autocorrect: false,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Password!';
                          }

                          return null;
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _onRegisterUser,
        child: Icon(Icons.person_add_alt_1),
      ),
    );
  }
}
