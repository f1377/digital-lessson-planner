import 'dart:io';

import 'package:digital_lesson_planner/model/user.dart';
import 'package:digital_lesson_planner/screens/overview_screen.dart';
import 'package:digital_lesson_planner/services/digital_lesson_planner_service.dart';
import 'package:digital_lesson_planner/services/secure_storage_service.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class UserSettingsScreen extends StatefulWidget {
  UserSettingsScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<UserSettingsScreen> createState() => _UserSettingsScreenState();
}

class _UserSettingsScreenState extends State<UserSettingsScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late String _userId;
  late User _loggedInUser;
  late final DigitalLessonPlannerService _service =
      Provider.of<DigitalLessonPlannerService>(context, listen: false);

  final SecureStorageService _secureStorageService = SecureStorageService();

  final TextEditingController _forenameController = TextEditingController();
  final TextEditingController _surnameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _isEdit = false;
  bool _usernameExists = false;

  bool _isObscure = true;
  Icon _isObscureIcon = Icon(Icons.remove_red_eye_outlined);

  String _forename = '';
  String _surname = '';
  String _email = '';
  String _username = '';
  String _password = '';

  final ImagePicker _imagePicker = ImagePicker();
  File? _image = File('');

  @override
  initState() {
    super.initState();
    _setUserVariables();
  }

  Future<void> _setUserVariables() async {
    final String? userId =
        await _secureStorageService.readKeyFromStorage('userId');

    if (userId == null || userId.isEmpty) {
      print('UserId not found!');
      return;
    }

    final User loggedInUser = _service.getUser(userId);

    _userId = userId;
    _loggedInUser = loggedInUser;

    _forename = loggedInUser.forename;
    _surname = loggedInUser.surname;
    _email = loggedInUser.email;
    _username = loggedInUser.username;
    _password = loggedInUser.password;

    if (loggedInUser.imagePath != null && loggedInUser.imagePath!.isNotEmpty) {
      _image = File(loggedInUser.imagePath!);
    }

    setState(() {});
  }

  Future<void> _loadUserImage() async {
    if (!_isEdit) {
      return;
    }

    final XFile? pickedImage =
        await _imagePicker.pickImage(source: ImageSource.gallery);

    if (pickedImage == null) {
      print('no file selected');
      return;
    }

    setState(() {
      _image = File(pickedImage.path);
    });
  }

  void _onShowPassword() {
    _isObscure = !_isObscure;

    if (_isObscure) {
      _isObscureIcon = Icon(Icons.remove_red_eye_outlined);
    } else {
      _isObscureIcon = Icon(Icons.remove_red_eye);
    }

    setState(() {});
  }

  void _onEditUser() {
    _isEdit = !_isEdit;
    _usernameExists = false;
    setState(() {});
  }

  Future<void> _onEditSave() async {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    _forename = _forenameController.text;
    _surname = _surnameController.text;
    _email = _emailController.text;
    _username = _usernameController.text;
    _password = _passwordController.text;

    final User updatedUser = User(
      id: _userId,
      forename: _forename,
      surname: _surname,
      email: _email,
      username: _username,
      password: _password,
    );

    if (_image != null && _image!.path.isNotEmpty) {
      updatedUser.imagePath = _image!.path;
    }
    await _service.updateUser(_userId, updatedUser);

    _isEdit = !_isEdit;
    setState(() {});
  }

  /* * TextFormField Widgets * */
  TextFormField _setForenameTextFormField() {
    return TextFormField(
      controller: _forenameController..text = _forename,
      validator: (String? value) {
        if (value == null || value.isEmpty) {
          return 'Please enter a valid Forename!';
        }
        return null;
      },
    );
  }

  TextFormField _setSurnameTextFormField() {
    return TextFormField(
      controller: _surnameController..text = _surname,
      validator: (String? value) {
        if (value == null || value.isEmpty) {
          return 'Please enter a valid Surname!';
        }
        return null;
      },
    );
  }

  TextFormField _setEmailTextFormField() {
    return TextFormField(
      controller: _emailController..text = _email,
      validator: (String? value) {
        if (value == null || value.isEmpty) {
          return 'Please enter a valid email!';
        }

        if (!RegExp(r'\S+@\S+\.\S+').hasMatch(value)) {
          return 'Please enter a valid Email!';
        }
        return null;
      },
    );
  }

  TextFormField _setUsernameTextFormField() {
    return TextFormField(
      controller: _usernameController..text = _username,
      onChanged: (String? value) async {
        if (value == null || value.isEmpty) {
          return;
        }

        if (value == _loggedInUser.username) {
          return;
        }

        _usernameExists = await _service.chekIfUsernameExists(value);
      },
      validator: (String? value) {
        if (value == null || value.isEmpty) {
          return 'Please enter a valid Username!';
        }

        if (_usernameExists) {
          _usernameExists = false;
          return 'Username already exits!';
        }

        return null;
      },
    );
  }

  TextFormField _setPasswordTextFormField() {
    return TextFormField(
      controller: _passwordController..text = _password,
      autocorrect: false,
      obscureText: true,
      enableSuggestions: false,
      validator: (String? value) {
        if (value == null || value.isEmpty) {
          return 'Please enter a valid password';
        }
        return null;
      },
    );
  }
  /* * TextFormField Widgets * */

  void _navigateToOverviewScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => OverviewScreen(userId: _userId),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: _navigateToOverviewScreen,
        ),
        title: Text("User Settings"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                GestureDetector(
                  onTap: _loadUserImage,
                  child: Container(
                    margin: const EdgeInsets.only(top: 20.0, bottom: 30.0),
                    height: 200.0,
                    width: 200.0,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38),
                      shape: BoxShape.circle,
                    ),
                    child: _image!.existsSync()
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(200),
                            child: Image.file(
                              _image!,
                              fit: BoxFit.cover,
                            ),
                          )
                        : Container(),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                      right: 8.0, bottom: 30.0, left: 8.0),
                  decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: Colors.black38)),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 40.0),
                  child: Column(
                    children: [
                      Card(
                        child: ListTile(
                          leading: const Icon(Icons.perm_identity),
                          title: _isEdit
                              ? _setForenameTextFormField()
                              : Text(_forename),
                          subtitle: Text('Forename'),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: const Icon(Icons.perm_identity_outlined),
                          title: _isEdit
                              ? _setSurnameTextFormField()
                              : Text(_surname),
                          subtitle: Text('Surname'),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: const Icon(Icons.email),
                          title:
                              _isEdit ? _setEmailTextFormField() : Text(_email),
                          subtitle: Text('Email'),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: const Icon(Icons.person),
                          title: _isEdit
                              ? _setUsernameTextFormField()
                              : Text(_username),
                          subtitle: Text('Username'),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: const Icon(Icons.password),
                          title: _isEdit
                              ? _setPasswordTextFormField()
                              : Text(_isObscure
                                  ? '${_password.replaceAll(RegExp(r"."), "*")}'
                                  : _password),
                          subtitle: Text('Password'),
                          trailing: _isEdit
                              ? null
                              : IconButton(
                                  icon: _isObscureIcon,
                                  onPressed: _onShowPassword,
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _isEdit ? _onEditSave : _onEditUser,
        child: _isEdit ? Icon(Icons.check) : Icon(Icons.edit),
      ),
    );
  }
}
