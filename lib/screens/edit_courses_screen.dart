import 'package:digital_lesson_planner/model/class.dart';
import 'package:digital_lesson_planner/model/lesson.dart';
import 'package:digital_lesson_planner/services/digital_lesson_planner_service.dart';
import 'package:digital_lesson_planner/widgets/headline.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'package:collection/collection.dart';

class EditCoursesScreen extends StatefulWidget {
  final String classId;
  EditCoursesScreen({
    Key? key,
    required this.classId,
  }) : super(key: key);

  @override
  State<EditCoursesScreen> createState() => _EditCoursesScreenState();
}

class _EditCoursesScreenState extends State<EditCoursesScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _classEditController = TextEditingController();

  late final DigitalLessonPlannerService _service =
      Provider.of<DigitalLessonPlannerService>(context, listen: false);
  late Class _classToEdit;

  List<Lesson> _lessonsList = [];

  late List<Dismissible> _lessons;

  @override
  void initState() {
    _classToEdit = _service.getClass(widget.classId);
    if (_classToEdit.lessons != null) {
      // clone list
      _lessonsList = [..._classToEdit.lessons!];
    }
    _createLessons();
    super.initState();
  }

  @override
  void dispose() {
    _classEditController.dispose();
    super.dispose();
  }

  void _createLessons() {
    _lessons = _lessonsList.mapIndexed((int index, Lesson lesson) {
      final String lessonNumber = (index + 1).toString();
      return _createLessonContainer(lesson.id, lessonNumber, lesson.text);
    }).toList();
  }

  void _classnameFieldEditFinished(String? value) {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    _service.updateClassname(_classToEdit.id, value!);
  }

  void _addLessonToListView() {
    setState(() {
      if (!_formKey.currentState!.validate()) {
        return;
      }

      final String id = Uuid().v4().toString();
      final Lesson newLesson = Lesson(id: id, text: "");

      _lessonsList.add(newLesson);
      _createLessons();
    });
  }

  void _lessonFieldEditFinished(String? value, String lessonId) {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    _service.addOrUpdateLesson(_classToEdit.id, lessonId, value!);
  }

  Container _buildAddLessonButton() {
    return Container(
      width: 40,
      alignment: Alignment.centerRight,
      child: Center(
        child: ElevatedButton(
          child: Icon(Icons.add),
          onPressed: _addLessonToListView,
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            padding: const EdgeInsets.all(0),
          ),
        ),
      ),
    );
  }

  Dismissible _createLessonContainer(
      String lessonId, String lessonNumber, String text) {
    return Dismissible(
      key: Key(lessonId),
      direction: DismissDirection.endToStart,
      onDismissed: (DismissDirection direction) {
        _lessonsList.removeWhere((lesson) => lesson.id == lessonId);
        _service.deleteLesson(_classToEdit.id, lessonId);
      },
      background: Container(
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: Text(
              "Delete",
              style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
      child: Card(
        child: ListTile(
          leading: Container(
            height: 30.0,
            width: 30.0,
            margin: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black38),
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Text(
                lessonNumber,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ),
          title: TextFormField(
            initialValue: text,
            decoration: InputDecoration(hintText: "Insert a Lesson"),
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter a valid Lesson!';
              }
              return null;
            },
            onFieldSubmitted: (String? value) {
              _lessonFieldEditFinished(value, lessonId);
            },
          ),
        ),
      ),
    );
  }

  Container _buildLessons() {
    return Container(
      padding: const EdgeInsets.all(8.0),
      margin: const EdgeInsets.only(bottom: 8.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black38),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: ListView(
        children: _lessons,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
        title: const Text("Edit Class"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 18.0, left: 8.0, right: 8.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(right: 4.0),
                child: TextFormField(
                  initialValue: _classToEdit.name,
                  decoration: InputDecoration(
                    icon: const Icon(Icons.article_outlined),
                    hintText: 'Insert a Classname',
                    labelText: 'Classname',
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter a valid Classname!';
                    }
                    return null;
                  },
                  onFieldSubmitted: _classnameFieldEditFinished,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: HeadLine.standard(
                        middleFlexWidth: 2,
                        title: "Lessons",
                      ),
                    ),
                  ),
                  _buildAddLessonButton(),
                ],
              ),
              Expanded(
                child: _buildLessons(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
