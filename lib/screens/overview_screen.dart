import 'dart:io';

import 'package:digital_lesson_planner/model/class.dart';
import 'package:digital_lesson_planner/model/lesson.dart';
import 'package:digital_lesson_planner/model/user.dart';
import 'package:digital_lesson_planner/screens/edit_courses_screen.dart';
import 'package:digital_lesson_planner/screens/login_screen.dart';
import 'package:digital_lesson_planner/screens/user_settings_screen.dart';
import 'package:digital_lesson_planner/services/digital_lesson_planner_service.dart';
import 'package:digital_lesson_planner/services/secure_storage_service.dart';
import 'package:digital_lesson_planner/widgets/headline.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:collection/collection.dart';

class OverviewScreen extends StatefulWidget {
  final String userId;

  const OverviewScreen({Key? key, required this.userId}) : super(key: key);

  @override
  _OverviewScreenState createState() => _OverviewScreenState();
}

class _OverviewScreenState extends State<OverviewScreen> {
  static const String _title = 'Digital Lesson Planner';
  int _navigationIndex = 0;

  final List<String> items = List<String>.generate(10, (i) => 'Item ${i + 1}');

  final GlobalKey<FormState> _classFormKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _searchFormKey = GlobalKey<FormState>();
  final TextEditingController _classnameController = TextEditingController();
  final TextEditingController _searchController = TextEditingController();

  final List<Map<String, dynamic>> _navigation = [
    {'label': "Plan", 'icon': Icons.edit},
    {'label': "Search", 'icon': Icons.search}
  ];

  late User _loggedInUser;
  late List<Class> _classes;
  List<Class> _searchClasses = [];

  final SecureStorageService _secureStorageService = SecureStorageService();
  late final DigitalLessonPlannerService _service =
      Provider.of<DigitalLessonPlannerService>(context, listen: false);

  String _userSurname = 'Surname';
  String _userForename = 'Forename';

  File? _image = File('');

  List<Widget> _navOptions = [];

  @override
  void initState() {
    super.initState();
    _classes = _getClasses();
    _setLoggedInUser();
  }

  @override
  void dispose() {
    _classnameController.dispose();
    super.dispose();
  }

  Future<void> _setLoggedInUser() async {
    final String? userId =
        await _secureStorageService.readKeyFromStorage('userId');

    if (userId != null && userId.isNotEmpty) {
      _loggedInUser = _getLoggedInUser();
      _setDrawerUserInfos();
      setState(() {});
    }
  }

  User _getLoggedInUser() {
    final User foundUser = _service.getUser(widget.userId);

    print("id: ${foundUser.id}");
    print("username: ${foundUser.username}");
    print("password: ${foundUser.password}");
    print("forename: ${foundUser.forename}");
    print("surname: ${foundUser.surname}");
    print("email: ${foundUser.email}");

    if (foundUser.imagePath != null && foundUser.imagePath!.isNotEmpty) {
      _image = File(foundUser.imagePath!);
    }

    return foundUser;
  }

  void _setDrawerUserInfos() {
    _userSurname = _loggedInUser.surname;
    _userForename = _loggedInUser.forename;
  }

  Future<void> _addClass() async {
    String classname = _classnameController.text;
    await _service.addClass(classname);
  }

  Future<void> _fillClasses(final BuildContext context) async {}

  List<Class> _getClasses() {
    final List<Class> classes = _service.getClasses();

    return classes;
  }

  AppBar _buildAppBar() {
    return AppBar(
      leading: Builder(
        builder: (BuildContext context) => IconButton(
          icon: Icon(Icons.tune_outlined),
          onPressed: () => Scaffold.of(context).openDrawer(),
        ),
      ),
      title: Text(_title),
      centerTitle: true,
      actions: const [
        Padding(
          padding: EdgeInsets.only(right: 20),
          child: Icon(Icons.menu_book),
        ),
      ],
    );
  }

  void _navigateToUserSettingsScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UserSettingsScreen(),
      ),
    );
  }

  Future<void> _exportToJson() async {
    final bool result = await _service.saveClassesAsJson(_classes);

    if (result) {
      showAlertPopUp('Success', 'Data exported');
    } else {
      showAlertPopUp('Warning', 'Nothing to export');
    }
  }

  Future<void> _importFromJson() async {
    final List<Class>? classes = await _service.readClassesFromFilePicker();

    if (classes == null) {
      print("File not selected");
      showAlertPopUp('Error', 'Data not found');
      return;
    }

    for (int i = 0; i < classes.length; i++) {
      print('id  [$i]: ${classes[i].id}');
      print('text[$i]: ${classes[i].name}');

      if (classes[i].lessons != null) {
        List<Lesson> lessons = classes[i].lessons!;

        for (int j = 0; j < lessons.length; j++) {
          print('id  [$j]: ${lessons[j].id}');
          print('name[$j]: ${lessons[j].text}');
        }
      }
    }

    setState(() {
      _classes = classes;
    });
    showAlertPopUp('Success', 'Data imported');
  }

  void showAlertPopUp(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: Text('OK'),
          ),
        ],
      ),
    );
  }

  void showSnackBarMessage(String message) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }

  Future<void> _onLogout() async {
    await _secureStorageService.setIsNotLoggedIn();

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ),
    );
  }

  Drawer _buildDrawer() {
    const borderRadius = BorderRadius.all(Radius.circular(16.0));
    final roundedRectangleBorder = RoundedRectangleBorder(
      borderRadius: borderRadius,
    );
    return Drawer(
      child: Container(
        margin: const EdgeInsets.only(top: 12.0),
        child: ListView(
          children: [
            DrawerHeader(
              padding: const EdgeInsets.only(
                right: 4.0,
                left: 4.0,
              ),
              child: Container(
                padding: const EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  color: Colors.orangeAccent,
                  borderRadius: borderRadius,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 8.0,
                                    left: 8.0,
                                  ),
                                  child: Text(
                                    _userSurname,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 24.0,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 8.0,
                                    left: 16.0,
                                  ),
                                  child: Text(
                                    _userForename,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              // width: 72.0,
                              child: Tooltip(
                                message: 'Log Out',
                                child: Card(
                                  child: ListTile(
                                    leading: const Icon(Icons.login_outlined),
                                    onTap: _onLogout,
                                    shape: roundedRectangleBorder,
                                  ),
                                  elevation: 4.0,
                                  shape: roundedRectangleBorder,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: _image!.existsSync()
                            ? Image.file(
                                _image!,
                                fit: BoxFit.cover,
                              )
                            : Center(
                                child: Icon(
                                  Icons.image,
                                  color: Colors.grey,
                                ),
                              ),
                      ),
                      width: 128.0,
                      height: 128.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: borderRadius,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Card(
              child: ListTile(
                leading: const Icon(Icons.person),
                title: const Text(
                  'User Settings',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onTap: _navigateToUserSettingsScreen,
                shape: RoundedRectangleBorder(
                  borderRadius: borderRadius,
                ),
              ),
              elevation: 4.0,
              shape: roundedRectangleBorder,
              color: Colors.orangeAccent,
            ),
            Card(
              child: ListTile(
                leading: const Icon(Icons.file_upload_outlined),
                title: const Text(
                  'Export',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onTap: _exportToJson,
                shape: roundedRectangleBorder,
              ),
              elevation: 4.0,
              shape: roundedRectangleBorder,
              color: Colors.orangeAccent,
            ),
            Card(
              child: ListTile(
                leading: const Icon(Icons.file_download_outlined),
                title: const Text(
                  'Import',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onTap: _importFromJson,
                shape: roundedRectangleBorder,
              ),
              elevation: 4.0,
              shape: roundedRectangleBorder,
              color: Colors.orangeAccent,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _navigateToEditCoursesScreen(
      BuildContext context, String classId) async {
    final bool backEventTriggered = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditCoursesScreen(classId: classId),
      ),
    );

    // back button was pressed
    if (backEventTriggered) {
      // reload classes
      setState(() {
        _classes = _getClasses();
      });
    }
  }

  Padding _buildPlanBodyInsertSection() {
    return Padding(
      padding: const EdgeInsets.only(left: 32.0, top: 32.0, bottom: 32.0),
      child: Form(
        key: _classFormKey,
        child: Row(
          children: [
            Expanded(
              flex: 4,
              child: TextFormField(
                controller: _classnameController,
                decoration: InputDecoration(
                  icon: const Icon(Icons.article_outlined),
                  hintText: 'Insert a Classname',
                  labelText: 'Add Class',
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a valid Classname!';
                  }

                  return null;
                },
              ),
            ),
            Expanded(
              child: ElevatedButton(
                child: Icon(Icons.add),
                onPressed: () async {
                  if (!_classFormKey.currentState!.validate()) {
                    return;
                  }

                  await _addClass();
                  setState(() {
                    _classes = _getClasses();
                  });
                },
                style: ElevatedButton.styleFrom(
                  shape: CircleBorder(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Card _createLessonCard(String lessonNumber, String text) {
    return Card(
      child: ListTile(
        leading: Container(
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black38),
            shape: BoxShape.circle,
          ),
          child: Center(
            child: Text(
              lessonNumber,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        title: Text(text),
      ),
    );
  }

  Container _buildBodyClassPanels() {
    return Container(
      height: 400,
      padding: const EdgeInsets.only(top: 8.0),
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ListView.builder(
        itemCount: _classes.length,
        itemBuilder: (BuildContext context, int index) {
          final currentClass = _classes[index];
          final List<Lesson>? lessons = currentClass.lessons;
          const String emptyLessonText = "Swipe right to add lessons";
          List<Widget> lessonsList = [ListTile(title: Text(emptyLessonText))];

          if (lessons != null) {
            lessonsList = lessons.mapIndexed((int lessonIndex, Lesson lesson) {
              final String lessonNumber = (lessonIndex + 1).toString();

              return _createLessonCard(lessonNumber, lesson.text);
            }).toList();
          }

          return Dismissible(
            confirmDismiss: (DismissDirection direction) async {
              // swipe right
              if (direction == DismissDirection.startToEnd) {
                _navigateToEditCoursesScreen(context, currentClass.id);
                return false;
              }
              return true;
            },
            key: Key(currentClass.name),
            onDismissed: (DismissDirection direction) {
              if (currentClass.id == null) {
                return;
              }

              setState(() {
                _classes.removeAt(index);
                _service.deleteClass(currentClass.id);
              });
              showSnackBarMessage('${currentClass.name} removed');
            },
            background: Container(
              decoration: BoxDecoration(
                color: Colors.lightBlueAccent,
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Text(
                    "Edit",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            secondaryBackground: Container(
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: Text(
                    "Delete",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            child: Card(
              child: ExpansionTile(
                leading: Icon(Icons.night_shelter),
                title: Text(currentClass.name),
                children: lessonsList,
              ),
            ),
          );
        },
      ),
    );
  }

  FutureBuilder _buildPlanBody() {
    return FutureBuilder(
      future: _fillClasses(context),
      builder: (context, snapshot) => SingleChildScrollView(
        child: Column(
          children: [
            _buildPlanBodyInsertSection(),
            HeadLine.standard(
              title: "Classes / Lessons",
              margin:
                  const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
              middleFlexWidth: 4,
            ),
            _buildBodyClassPanels(),
          ],
        ),
      ),
    );
  }

  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      items: _navigation
          .map(
            (element) => BottomNavigationBarItem(
              label: element['label'],
              icon: Icon(element['icon']),
            ),
          )
          .toList(),
      onTap: (index) {
        this.setState(() {
          _navigationIndex = index;
        });
      },
      currentIndex: _navigationIndex,
    );
  }

  Future<void> _onSearch() async {
    if (!_searchFormKey.currentState!.validate()) {
      return;
    }

    _searchClasses =
        await _service.searchInClassAndLessons(_searchController.text);

    setState(() {});
    if (_searchClasses.isEmpty) {
      showSnackBarMessage('Found nothing !!!');
      return;
    }
  }

  Padding _buildSearchBodyFieldSection() {
    return Padding(
      padding: const EdgeInsets.only(left: 32.0, top: 32.0, bottom: 32.0),
      child: Form(
        key: _searchFormKey,
        child: Row(
          children: [
            Expanded(
              flex: 4,
              child: TextFormField(
                controller: _searchController,
                decoration: InputDecoration(
                  icon: const Icon(Icons.search_sharp),
                  hintText: 'New Search...',
                  labelText: 'Search',
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'No Input!';
                  }

                  return null;
                },
              ),
            ),
            Expanded(
              child: ElevatedButton(
                child: Icon(Icons.search),
                onPressed: _onSearch,
                style: ElevatedButton.styleFrom(
                  shape: CircleBorder(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container _buildSearchBodyClassPanels() {
    return Container(
      height: 400.0,
      padding: const EdgeInsets.only(top: 8.0),
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ListView.builder(
        itemCount: _searchClasses.length,
        itemBuilder: (BuildContext context, int index) {
          final currentClass = _searchClasses[index];
          final List<Lesson>? lessons = currentClass.lessons;
          List<Widget> lessonsList = [];

          if (lessons != null) {
            lessonsList = lessons.mapIndexed((int lessonIndex, Lesson lesson) {
              final String lessonNumber = (lessonIndex + 1).toString();

              return _createLessonCard(lessonNumber, lesson.text);
            }).toList();
          }

          return Card(
            child: ExpansionTile(
              leading: Icon(Icons.search),
              title: Text(currentClass.name),
              children: lessonsList,
            ),
          );
        },
      ),
    );
  }

  Container _buildSearchBody() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildSearchBodyFieldSection(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: HeadLine.standard(title: ''),
            ),
            _buildSearchBodyClassPanels(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _navOptions = [
      _buildPlanBody(),
      _buildSearchBody(),
    ];

    return Scaffold(
      appBar: _buildAppBar(),
      drawer: _buildDrawer(),
      body: _navOptions.elementAt(_navigationIndex),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }
}
